let createCourse = document.querySelector('#registerUser');
	

//add event listener
createCourse.addEventListener("submit", (e) => {
	e.preventDefault();

	let courseName = document.getElementById('courseName').value;
	let coursePrice = document.getElementById('coursePrice').value;
	let courseDesc = document.getElementById('courseDesc').value;
	

if(courseName !== "" && coursePrice !== "" &&  courseDesc !== "" )
	{
		fetch("http://localhost:3000/api/users/addCourse",
				{
					method: "POST",
					headers: {
						"Content-Type": "application/json",
						"Authorization": `Bearer ${token}`
					},
					body: JSON.stringify({
						name: courseName,
						description: courseDesc,
						price: coursePrice
					})
				})
				.then(result => result.json())
				.then(result => {
					if (result === true) {
						alert("Added Course Successfully");

						window.location.replace('/course.html');
					} else {
						alert("Something went wrong.");
					}
				})

			} else {
				alert("Please input all fields");


	}
