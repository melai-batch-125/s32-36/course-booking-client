//target
let loginForm = document.querySelector('#loginUser');


loginForm.addEventListener("submit", (e) => {
	e.preventDefault();

	let email = document.getElementById('email').value;
	let password = document.getElementById('password').value;

	fetch("http://localhost:3000/api/users/login", 
		{
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})



		})
	.then(result => result.json())
	.then(result => {
		localStorage.setItem("token", result.access);

		if(result.access){
			fetch("http://localhost:3000/api/users/details", 
			{
				method: "GET",
				headers: {
					"Authorization": `Bearer ${result.access}`
				}
			}
		}
	})
			.then(result => result.json())
				.then(result => {

					console.log(result)

					localStorage.setItem("id", result._id);
					localStorage.setItem("isAdmin", result.isAdmin);

					window.location.replace('/courses.html')


				})


})