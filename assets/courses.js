let token = localStorage.getItem("token");
let adminUser = localStorage.getItem('isAdmin') === "true";



let admin = document.querySelector(#adminButton);

if (adminUser === false || adminUser === null) {
	adminButton.innerHTML = null;
} else {
	adminButton.innerHTML =
	`
	<div>
		<a href="./addCourse.html" class="btn btn-block btn-primary"></a>
	</div>

	`
}

fetch("http://localhost:3000/api/users/all",
{
	method: "GET",
	headers: {
		"Content-Type": "application/json",
		"Authorization": `Bearer ${token}`
	}
})
.then(result => result.json())
.then(result => {
	if (result.length < 1) {
		courseData = `No Courses Available`;
		// alert("Registered Successfully");

		// window.location.replace('/login.html');
	} else {
		courseData = result.map((course) => {
			console.log(course);
			return (
				`
				<div class="col-md-6 my-5">
					<div class = "card">
						<div class="card-body">
							<h5 class="card-title">
								${course.name}
							</h5>
							<p class="card-text text-left">
								${course.description}
							</p>
							<p class="card-text text-right">
								${course.price}
							</p>
						</div>
						<div class="card-footer">

						</div>
					</div>
				</div>
				`
				)
		});
		// alert("Something went wrong.");
	}

	let container =document.querySelector('#courseContainer');

	container.innerHTML = courseData
	
})



