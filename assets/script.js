//DOM manipulation

let navSession = document.querySelector('#navSession');
let registerLink = document.querySelector('#register');
let userToken = localStorage.getItem('token')

if(!userToken){

	navSession.innerHTML = 
	`
		<li class="nav-item">
			<a href="./login.html" class="nav-link">Login</a>
		</li>
	`

	registerLink.innerHTML =
	`
		<li class="nav-item">
			<a href="./register.html" class="nav-link">Register</a>
		</li>
	`
} else {

	navSession.innerHTML =
	`
		<li class="nav-item">
			<a href="./logout.html" class="nav-link">Logout</a>
		</li>
	`
}
